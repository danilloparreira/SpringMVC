package br.com.caelum.loja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.caelum.loja.dao.ProdutoDAO;
import br.com.caelum.loja.models.Produto;

@Controller
public class ProdutosController {

	@Autowired
	private ProdutoDAO produtoDAO;

	@RequestMapping("/produtos/form")
	public String formulario() {
		return "produtos/form";
	}

	@RequestMapping("/produtos")
	public String grava(Produto produto) {
		produtoDAO.grava(produto);
		return "produtos/ok";
	}
}