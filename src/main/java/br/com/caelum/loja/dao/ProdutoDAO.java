package br.com.caelum.loja.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.caelum.loja.models.Produto;

@Repository
@Transactional
public class ProdutoDAO {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void grava(Produto produto) {
		manager.persist(produto);
	}

	@Transactional
	public void edita(Produto produto) {
		manager.merge(produto);
	}

	@Transactional
	public void remove(Produto produto) {
		manager.remove(produto);
	}

	@Transactional
	public Produto busca(int id) {
		return manager.find(Produto.class, id);
	}

}